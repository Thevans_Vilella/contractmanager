﻿using ContractManager1._0.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ContractManager1._0.Dal
{
    public class ContractManagerContext : IdentityDbContext
    {
        public DbSet<Contract> Contract { get; set; }

        public ContractManagerContext(DbContextOptions<ContractManagerContext> options) : base(options)
        {
        }
    }
}
