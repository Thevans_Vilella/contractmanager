﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ContractManager1._0.Models
{
    public class Contract
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [DisplayName("Nome do cliente")]
        public string ClientName { get; set; }

        [DisplayName("Tipo do contrato")]
        public ContractType Type { get; set; }

        [DisplayName("Quantidade negociada")]
        public int NegotiatedQuantity { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        [DataType(DataType.Currency)]
        [DisplayName("Valor negociado")]
        public decimal NegotiatedValue { get; set; }

        [DisplayName("Mês/Ano inicio do contrato")]
        [DisplayFormat(DataFormatString = "{0:MM/yyyy}", ApplyFormatInEditMode = true)]
        [DataType(DataType.Date)]
        public DateTime ContractStart { get; set; }

        [DisplayName("Duração do contrato (meses)")]
        public int Duration { get; set; }

        [DisplayName("PDF do contrato")]
        public string File { get; set; }
    }
}
